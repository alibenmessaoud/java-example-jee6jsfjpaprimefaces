package formation.jee6;

import javax.annotation.Resource;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.sql.DataSource;
import lombok.extern.slf4j.Slf4j;

/**
 * Create un-managed (third party libs) beans.
 *
 * @author herbie
 */
@Slf4j
public class ProducerBean {

    @Resource(name = "jdbc/WEBSTORE")
    private DataSource webStoreDatasource;
    
    @Inject
    @Config
    private String indexdir;

//    @Produces
//    public XXX createXXXL() {
//    }

}
