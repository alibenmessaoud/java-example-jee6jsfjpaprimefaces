package formation.jee6.services;

import formation.jee6.model.Product;
import java.util.List;
import javax.ejb.Singleton;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

@Singleton
@Named("productServices")
public class ProductServices {

   
    @PersistenceContext
    EntityManager em;
    
    public List<Product> getProducts() {
        return em.createQuery("select p from Product p").getResultList();
    }
    
    public void createProductInDB() {
        em.persist(new Product("car","Une voiture",25));
    }
}
