package formation.jee6.controllers.login;

import formation.jee6.services.ProductServices;
import java.io.Serializable;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

/**
 * Statefull session bean.
 *
 * @author snichele
 */
@Named("loginController")
@SessionScoped
@Slf4j
@Getter
@Setter
public class LoginController implements Serializable {

    @Inject
    transient ProductServices service;
    
    private String login;
    private String password;
    
    private boolean userAuthentified = false;

    public LoginController() {
        log.debug("New LoginController created.");
    }

    public void authenticate() {
        if(login.equals("admin") && password.equals("admin")) {
            userAuthentified = true;
        }
    }
}
