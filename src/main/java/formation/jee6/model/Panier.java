package formation.jee6.model;

import javax.ejb.Stateful;
import javax.inject.Named;

@Stateful
@Named("panier")
public class Panier {
    private Product currentSelectedProduct;

    public Product getCurrentSelectedProduct() {
        return currentSelectedProduct;
    }

    public void setCurrentSelectedProduct(Product currentSelectedProduct) {
        this.currentSelectedProduct = currentSelectedProduct;
    }
}
