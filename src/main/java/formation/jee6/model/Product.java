package formation.jee6.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "JEE6Products")
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String description;
    private int quantityAvailable;

    public Product() {
    }

    public Product(String name, String description, int quantityAvailable) {
        this.name = name;
        this.description = description;
        this.quantityAvailable = quantityAvailable;
    }
}
