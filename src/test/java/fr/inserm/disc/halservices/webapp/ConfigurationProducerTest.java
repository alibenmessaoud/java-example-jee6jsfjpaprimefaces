package fr.inserm.disc.halservices.webapp;

import formation.jee6.Config;
import javax.inject.Inject;
import org.junit.Test;

public class ConfigurationProducerTest extends BaseCDITest{

    public ConfigurationProducerTest() {
    }

    /**
     * Test of getConfiguration method, of class ConfigurationProducer.
     */
    @Test
    public void testGetConfiguration() {
        System.out.println("getConfiguration");
        SomeConfigurableClass i = new SomeConfigurableClass();
        System.out.println(i.getSome());
    }

    private static class SomeConfigurableClass {

        @Inject
        @Config
        private String some;

        public String getSome() {
            return some;
        }

    }
}
